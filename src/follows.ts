import { Arguments } from "./arguments";
import { Matchers } from "./matchers";

export function follows<TTransformer extends (...nodes: any[]) => any>(
  transformer: TTransformer
): (
  ...patterns: Matchers<Arguments<TTransformer>>
) => (
  input: string
) => false | { result: ReturnType<TTransformer>; consumed: number } {
  return (...patterns) => input => {
    const matches: any[] = [];
    let remaining = input;
    let consumed = 0;

    for (const pattern of patterns) {
      const matched = pattern(remaining);

      if (matched === false) {
        return false;
      }

      matches.push(matched);
      consumed += matched.consumed;
      remaining = input.slice(consumed);
    }

    return {
      consumed,
      result: transformer(...matches.map(match => match.result))
    };
  };
}
