import { Arguments } from "./arguments";
import { just } from "./just";

export function nOrMore(
  n: number
): <TTransformer extends (...nodes: any[]) => any>(
  transformer: TTransformer
) => <
  TPattern extends (
    input: string
  ) =>
    | false
    | {
        result: Arguments<TTransformer>[0] extends Array<infer U> ? U : never;
        consumed: number;
      }
>(
  pattern: TPattern
) => (
  input: string
) => false | { result: ReturnType<TTransformer>; consumed: number } {
  return transformer => pattern => input => {
    let consumed = 0;
    let remaining = input;
    const matches: any[] = [];

    while (remaining.length > 0) {
      const matched = pattern(remaining);

      if (matched === false || matched.consumed === 0) {
        break;
      }

      consumed += matched.consumed;
      remaining = remaining.slice(matched.consumed);
    }

    return (
      matches.length >= n && {
        consumed,
        result: transformer(matches.map(match => match.result))
      }
    );
  };
}
