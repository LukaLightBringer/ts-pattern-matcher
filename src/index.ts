import { nOrMore } from "./nOrMore";
import { cases } from "./cases";
import { follows } from "./follows";
import { just } from "./just";

const zeroOrMore = nOrMore(0);
const literal = cases(just(s => s)(""));
