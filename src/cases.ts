export function cases<
  TInput,
  TPatterns extends Array<
    (input: TInput) => false | { result: any; consumed: number }
  >
>(
  ...patterns: TPatterns
): (
  input: TInput
) =>
  | false
  | (TPatterns extends Array<infer U>
      ? (U extends (input: TInput) => false | { result: any; consumed: number }
          ? ReturnType<U>
          : never)
      : never) {
  return input =>
    patterns
      .map(pattern => pattern(input))
      .reduce(
        (a, b) =>
          a === false ? b : b === false ? a : a.consumed > b.consumed ? a : b,
        false
      ) as any;
}
