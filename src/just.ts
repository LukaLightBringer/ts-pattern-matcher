export function just<TTransformer extends (string: string) => any>(
  transformer: TTransformer
): (
  target: string
) => (
  input: string
) => false | { result: ReturnType<TTransformer>; consumed: number } {
  return target => input =>
    input.startsWith(target) && {
      consumed: target.length,
      result: transformer(target)
    };
}
