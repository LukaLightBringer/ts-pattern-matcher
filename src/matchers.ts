export type Matchers<T extends any[]> = {
  [P in keyof T]: (value: string) => false | { consumed: number; result: T[P] }
} &
  any[];
