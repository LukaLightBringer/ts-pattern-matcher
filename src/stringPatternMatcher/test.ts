import { stringPatternMatcher } from "./index";
const { follows, just, cases, zeroOrMore } = stringPatternMatcher;

const balanced = (
  input: string
): false | { consumed: number; result: string } =>
  zeroOrMore(
    cases(
      follows(just("("), balanced, just(")")),
      follows(just("{"), balanced, just("}")),
      follows(just("["), balanced, just("]")),
      follows(just("<"), balanced, just(">"))
    )
  )(input);

function entirely(
  pattern: (value: string) => false | { consumed: number; result: string }
): (input: string) => boolean {
  return input => {
    const match = pattern(input);
    return match !== false && match.consumed === input.length;
  };
}

const entirelyBalanced = entirely(balanced);
function check(input: string) {
  console.log(`"${input}" is ${entirelyBalanced(input) ? "" : "not "}balanced`);
}

check("()");
check("<>");
check("{}");
check("[]");
check("(<>[])");
check("(<)>");
check("()<]");
