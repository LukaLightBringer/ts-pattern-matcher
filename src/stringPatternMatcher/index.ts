import { follows } from "../follows";
import { just } from "../just";
import { cases } from "../cases";
import { nOrMore } from "../nOrMore";

export const stringPatternMatcher = {
  follows: follows((...strings: string[]) => strings.join("")),
  just: just((string: string) => string),
  cases,
  zeroOrMore: nOrMore(0)((strings: string[]) => strings.join("")),
  oneOrMore: nOrMore(1)((strings: string[]) => strings.join(""))
};
